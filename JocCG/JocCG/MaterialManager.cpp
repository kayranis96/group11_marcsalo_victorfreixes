#include "MaterialManager.h"



void MaterialManager::createMaterialDefinitions()
{
	_materialData[JADE].ambient = glm::vec3(0.135, 0.2225, 0.1575);
	_materialData[JADE].diffuse = glm::vec3(0.54, 0.89, 0.63);
	_materialData[JADE].specular = glm::vec3(0.316228, 0.316228, 0.316228);
	_materialData[JADE].shininess = 0.1f*128.0f;
	_materialType[JADE] = "jade";

	_materialData[CHROME].ambient = glm::vec3(0.25,0.25,0.25);
	_materialData[CHROME].diffuse = glm::vec3(0.4,0.4,0.4);
	_materialData[CHROME].specular = glm::vec3(0.774597, 0.774597, 0.774597);
	_materialData[CHROME].shininess = 0.6f*128.0f;
	_materialType[CHROME] = "chrome";

	_materialData[SILVER].ambient = glm::vec3(0.19225, 0.19225, 0.19225);
	_materialData[SILVER].diffuse = glm::vec3(0.50754, 0.50754, 0.50754);
	_materialData[SILVER].specular = glm::vec3(0.508273, 0.508273, 0.508273);
	_materialData[SILVER].shininess = 0.4f*128.0f;
	_materialType[SILVER] = "silver";

	_materialData[BRASS].ambient = glm::vec3(0.329412, 0.223529, 0.027451);
	_materialData[BRASS].diffuse = glm::vec3(0.780392, 0.568627, 0.113725);
	_materialData[BRASS].specular = glm::vec3(0.992157, 0.941176, 0.807843);
	_materialData[BRASS].shininess = 0.21794872f*128.0f;
	_materialType[BRASS] = "brass";

	_materialData[RUBY].ambient = glm::vec3(0.1745, 0.01175, 0.01175);
	_materialData[RUBY].diffuse = glm::vec3(0.61424, 0.04136, 0.04136);
	_materialData[RUBY].specular = glm::vec3(0.727811, 0.626959, 0.626959);
	_materialData[RUBY].shininess = 0.6f*128.0f;
	_materialType[RUBY] = "ruby";

	_materialData[LIGHT].ambient = glm::vec3(1,1,1);
	_materialData[LIGHT].diffuse = glm::vec3(1, 1, 1);
	_materialData[LIGHT].specular = glm::vec3(1, 1, 1);
	_materialData[LIGHT].shininess = 1.0f*128.0f;
	_materialData[LIGHT].attenuation = glm::vec3(0.01,0.01,0.01);
	_materialType[LIGHT] = "light";

}

MaterialManager::MaterialManager()
{
	createMaterialDefinitions();
}


MaterialManager::~MaterialManager()
{
}

material MaterialManager::getMaterialComponents(int materialID)
{
	return _materialData[materialID];
}

int MaterialManager::getMaterialID(std::string materialName)
{
	for (int i = 0; i < NUMMATERIALS;i++) {
		if (_materialType[i] == materialName) {
			return i;
		}
	}
	return -1;
}
