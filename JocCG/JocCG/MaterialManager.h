#pragma once
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <string>

#define JADE 0
#define CHROME 1 
#define SILVER 2
#define BRASS 3
#define RUBY 4
#define LIGHT 5
#define NUMMATERIALS 6

struct material {
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
	float shininess;
	glm::vec3 attenuation;
};

class MaterialManager
{
	material _materialData[NUMMATERIALS];
	std::string _materialType[NUMMATERIALS];
	void createMaterialDefinitions();
public:
	MaterialManager();
	~MaterialManager();
	material getMaterialComponents(int materialID);
	int getMaterialID(std::string materialName);
};

