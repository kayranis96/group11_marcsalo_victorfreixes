#pragma once
#include "Vertex.h"
#include <vector>
#include "GameObject.h"
#include "ObjectLoader.h"

#define GREEN 0
#define RED 1
#define WHITE 2
#define CUBE 0
#define TEAPOT 1
#define OBJ 2

#define NUMBASICOBJECTS 1

//This class stores and manipulates all the objects loaded from the text file
class Geometry
{
	std::vector<Vertex*> _verticesData;
	std::vector<int> _numVertices;
	std::vector <GameObject> _listOfObjects;
	ObjectLoader _loader;
	

public:
	Geometry();
	~Geometry();
	void loadGameElements(char fileName[100]);
	Vertex * getData(int objectID);
	int getNumVertices(int objectID);
	int getNumGameElements();
	GameObject & getGameElement(int objectID);
};

