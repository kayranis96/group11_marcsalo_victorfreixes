#include "Game.h"


/**
* Constructor
* Note: It uses an initialization list to set the parameters
* @param windowTitle is the window title
* @param screenWidth is the window width
* @param screenHeight is the window height
*/
Game::Game(std::string windowTitle, int screenWidth, int screenHeight, bool enableLimiterFPS, int maxFPS, bool printFPS) :
	_windowTitle(windowTitle), 
	_screenWidth(screenWidth), 
	_screenHeight(screenHeight),
	_gameState(GameState::INIT), 
	_fpsLimiter(enableLimiterFPS, maxFPS, printFPS) {


}

/**
* Destructor
*/
Game::~Game()
{
}

/*
* Game execution
*/
void Game::run() {
		//System initializations
	initSystems();
		//Start the game if all the elements are ready
	gameLoop();
}

/*
* Initializes all the game engine components
*/

void Game::loadGameTextures() {

	/*Suggestion:
	- You can define the texture fileNames in the "scene3D.txt" file.
	- Next, you can loop through the GameObjects stored in the Geometry class, and assign the textureID in each GameObject
	*/

	GameObject currentGameObject;
	//Load the game textures			
	for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
		currentGameObject = _gameElements.getGameElement(i);
		(_gameElements.getGameElement(i))._textureID = _textureManager.getTextureID(currentGameObject._textureFile);
	}


}

void Game::initSystems() {
		//Create an Opengl window using SDL
	_window.create(_windowTitle, _screenWidth, _screenHeight, 0);		
		//Compile and Link shader
	initShaders();
		//Set up the openGL buffers
	_openGLBuffers.initializeBuffers(_colorProgram);

		//Load the current scenario
	//_gameElements.loadGameElements("./resources/scene2D.txt");
	(_gameElements.getGameElement(0))._velocity = glm::vec3(0.5, 0, 0);

	//Load the game textures
	loadGameTextures();

	///CAM
	_cam.setNear(0.1f);
	_cam.setFar(30.0f);
	_cam.setProjectionWidth(30.0f);
	_cam.setProjectionHeight(30.0f);
	_cam.setFov(45.0f);
	_cam.setCameraPos(glm::vec3(-2.0f,0.0f,0.5f));
	_cam.setCameraFront(glm::vec3(0.0f, 0.0f, 0.0f));
	_cam.setCameraUp(glm::vec3(0.0f, 0.0f, 1.0f));
	_cam.setAspectRatio((float)(_screenWidth / _screenHeight));


	for (int i = 1; i < _gameElements.getNumGameElements() - 1;i++) {
		float velX = ((rand() % 200) - 100)*0.1f;
		float velY = ((rand() % 200) - 100)*0.1f;
		float velZ = ((rand() % 200) - 100)*0.1f;
		(_gameElements.getGameElement(i))._velocity = glm::vec3(velX, velY, velZ);
	}


	_planeBottom.setPointNormal(glm::vec3(0.0f, 0.0f, -0.9f), glm::vec3(0.0f, 0.0f, -1.0f));
	_planeTop.setPointNormal(glm::vec3(0.0f, 0.0f, 2.0f), glm::vec3(0.0f, 0.0f, -1.0f));
	_planeRight.setPointNormal(glm::vec3(0.0f, 10.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f));
	_planeLeft.setPointNormal(glm::vec3(0.0f, -10.0f, -0.0f), glm::vec3(0.0f, -1.0f, 0.0f));
	_planeFront.setPointNormal(glm::vec3(-10.0f, 0.0f, -0.0f), glm::vec3(-1.0f, 0.0f, 0.0f));
	_planeBack.setPointNormal(glm::vec3(10.0f, 0.0f, -0.0f), glm::vec3(-1.0f, 0.0f, 0.0f));


	
}

/*
* Initialize the shaders:
* Compiles, sets the variables between C++ and the Shader program and links the shader program
*/
void Game::initShaders() {
	//Compile the shaders
	_colorProgram.addShader(GL_VERTEX_SHADER, "./resources/shaders/vertex-shader.txt");
	_colorProgram.addShader(GL_FRAGMENT_SHADER, "./resources/shaders/fragment-shader.txt");
	_colorProgram.compileShaders();
	//Attributes must be added before linking the code
	_colorProgram.addAttribute("vertexPositionGame");
	//_colorProgram.addAttribute("vertexColor");
	_colorProgram.addAttribute("vertexUV");
	_colorProgram.addAttribute("vertexNormal");
	//Link the compiled shaders
	_colorProgram.linkShaders();

	//Uniform variables send to the shader
	_modelMatrixUniform = _colorProgram.getUniformLocation("modelMatrix");
	_modelNormalMatrixUniform = _colorProgram.getUniformLocation("modelNormalMatrix");
	_viewMatrixUniform = _colorProgram.getUniformLocation("viewMatrix");
	_projectionMatrixUniform = _colorProgram.getUniformLocation("projectionMatrix");
	_newColorUniform = _colorProgram.getUniformLocation("objectColor");
	_textureDataLocation = _colorProgram.getUniformLocation("textureData");
	_textureScaleFactorLocation = _colorProgram.getUniformLocation("textureScaleFactor");
	_materialAmbientUniform = _colorProgram.getUniformLocation("material.ambient");
	_materialDiffuseUniform = _colorProgram.getUniformLocation("material.diffuse");
	_materialSpecularUniform = _colorProgram.getUniformLocation("material.specular");
	_materialShininessUniform = _colorProgram.getUniformLocation("material.shininess");
	_isALightSource = _colorProgram.getUniformLocation("isALightSource");
	_lightingEnabled = _colorProgram.getUniformLocation("lightingEnabled");
	_lightPosition = _colorProgram.getUniformLocation("lightPosition");
	_lightPosition2 = _colorProgram.getUniformLocation("lightPosition2");
	_viewerPosition = _colorProgram.getUniformLocation("viewerPosition");
	//light1
	_lightAmbientUniform = _colorProgram.getUniformLocation("lightColor.ambient");
	_lightDiffuseUniform = _colorProgram.getUniformLocation("lightColor.diffuse");
	_lightSpecularUniform = _colorProgram.getUniformLocation("lightColor.specular");
	_lightAttenuationUniform = _colorProgram.getUniformLocation("lightColor.attenuation");
	//light2
	_lightAmbientUniform2 = _colorProgram.getUniformLocation("lightColor2.ambient");
	_lightDiffuseUniform2 = _colorProgram.getUniformLocation("lightColor2.diffuse");
	_lightSpecularUniform2 = _colorProgram.getUniformLocation("lightColor2.specular");
	_lightAttenuationUniform2 = _colorProgram.getUniformLocation("lightColor2.attenuation");

	_lightMode = _colorProgram.getUniformLocation("lightMode");
}

/*
* Game execution: Gets input events, processes game logic and draws sprites on the screen
*/
void Game::gameLoop() {	
	_gameState = GameState::PLAY;
	while (_gameState != GameState::EXIT) {		
			//Start synchronization between refresh rate and frame rate
		_fpsLimiter.startSynchronization();
			//Process the input information (keyboard and mouse)
		processInput();
			//Execute the player actions (keyboard and mouse)
		executePlayerCommands();
			//Update the game status
		doPhysics();
			//Draw the objects on the screen
		renderGame();	
			//Force synchronization
		_fpsLimiter.forceSynchronization();
	}
	std::cout << "YOU DIED" << std::endl;
	std::cout << "Your fucking score is: " << score << std::endl;
	system("pause");
}

/*
* Processes input with SDL
*/
void Game::processInput() {
	_inputManager.update();
	//Review https://wiki.libsdl.org/SDL_Event to see the different kind of events
	//Moreover, table show the property affected for each event type
	SDL_Event evnt;
	//Will keep looping until there are no more events to process
	while (SDL_PollEvent(&evnt)) {
		switch (evnt.type) {
		case SDL_QUIT:
			_gameState = GameState::EXIT;
			break;
		case SDL_MOUSEMOTION:
			_inputManager.setMouseCoords(evnt.motion.x, evnt.motion.y);
			break;
		case SDL_KEYDOWN:
			_inputManager.pressKey(evnt.key.keysym.sym);
			break;
		case SDL_KEYUP:
			_inputManager.releaseKey(evnt.key.keysym.sym);
			break;
		case SDL_MOUSEBUTTONDOWN:
			_inputManager.pressKey(evnt.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			_inputManager.releaseKey(evnt.button.button);
			break;
		default:
			break;
		}
	}

}


/**
* Executes the actions sent by the user by means of the keyboard and mouse
*/
void Game::executePlayerCommands() {
	if (_inputManager.isKeyDown(SDL_BUTTON_LEFT)){
		glm::ivec2 mouseCoords = _inputManager.getMouseCoords();
		cout << mouseCoords.x << ", " << mouseCoords.y << endl;
		
	}

	if (_inputManager.isKeyDown(SDLK_1)) {
		typeOfCamera = 0;
	}

	if (_inputManager.isKeyDown(SDLK_2)) {
		typeOfCamera = 1;
	}

	if (_inputManager.isKeyDown(SDLK_w)) {
		(_gameElements.getGameElement(0))._translate.x -= 0.05f;
	}


	if (_inputManager.isKeyDown(SDLK_s)) {
		(_gameElements.getGameElement(0))._translate.x += 0.05f;
	}

	if (_inputManager.isKeyDown(SDLK_a)) {
		(_gameElements.getGameElement(0))._translate.y -= 0.05f;
	}
	

	if (_inputManager.isKeyDown(SDLK_d)) {
		(_gameElements.getGameElement(0))._translate.y += 0.05f;
	}

	if (_inputManager.isKeyPressed(SDLK_l)) {
		if (_lightSwitch == 0) {
			_lightSwitch = 1;
		}
		else {
			_lightSwitch = 0;
		}
	}

	if (_inputManager.isKeyPressed(SDLK_m)) {
		if (_lightModeSwitch == 0) {
			_lightModeSwitch = 1;
			std::cout << "pistash \n";
		}
		else {
			_lightModeSwitch = 0;
			std::cout << "pistosh \n";
		}
	}

	if (_inputManager.isKeyPressed(SDLK_ESCAPE)) {
		_gameState = GameState::EXIT;
	}
	

}

/*
* Update the game objects based on the physics
*/
void Game::doPhysics() {
	
	float disact, disantPBo, disantPT, disantPR, disantPL, disantPF, disantPBa;

	


	for (int i = 0; i < _gameElements.getNumGameElements()-2;i++) {
		
		disantPBo = _planeBottom.distPoint2Plane(_gameElements.getGameElement(i)._translate) + 0.05f;
		disantPT = _planeTop.distPoint2Plane(_gameElements.getGameElement(i)._translate) + 0.05f;
		disantPR = _planeRight.distPoint2Plane(_gameElements.getGameElement(i)._translate) + 0.05f;
		disantPL = _planeLeft.distPoint2Plane(_gameElements.getGameElement(i)._translate) + 0.05f;
		disantPF = _planeFront.distPoint2Plane(_gameElements.getGameElement(i)._translate) + 0.05f;
		disantPBa = _planeBack.distPoint2Plane(_gameElements.getGameElement(i)._translate) + 0.05f;
		
		_gameElements.getGameElement(i)._force = glm::vec3(0);
		_gameElements.getGameElement(i)._force += _gravity;
		//EulerSemi
		_gameElements.getGameElement(i)._previousTranslate = _gameElements.getGameElement(i)._translate;
		(_gameElements.getGameElement(i))._velocity += _gameElements.getGameElement(i)._force*_dt;
		_gameElements.getGameElement(i)._translate += (_gameElements.getGameElement(i))._velocity*_dt;

		glm::vec3 correcPos;
		glm::vec3 correcVel;

		disact = _planeBottom.distPoint2Plane(_gameElements.getGameElement(i)._translate) + 0.05f;
		
		if (disantPBo*disact < 0.0f) {
			//system("pause");
			correcPos = -(1.0f + 1.0f) * disact * _planeBottom.normal;
			correcVel = -(1.0f + 1.0f) * ((_gameElements.getGameElement(i))._velocity*_planeBottom.normal)*_planeBottom.normal;
			_gameElements.getGameElement(i)._translate += correcPos;
			_gameElements.getGameElement(i)._velocity += correcVel;
		}

		disact = _planeTop.distPoint2Plane(_gameElements.getGameElement(i)._translate) + 0.05f;

		if (disantPT*disact < 0.0f) {
			//system("pause");
			correcPos = -(1.0f + 1.0f) * disact * _planeTop.normal;
			correcVel = -(1.0f + 1.0f) * ((_gameElements.getGameElement(i))._velocity*_planeTop.normal)*_planeTop.normal;
			_gameElements.getGameElement(i)._translate += correcPos;
			_gameElements.getGameElement(i)._velocity += correcVel;
		}

		disact = _planeRight.distPoint2Plane(_gameElements.getGameElement(i)._translate) + 0.05f;

		if (disantPR*disact < 0.0f) {
			//system("pause");
			correcPos = -(1.0f + 1.0f) * disact * _planeRight.normal;
			correcVel = -(1.0f + 1.0f) * ((_gameElements.getGameElement(i))._velocity*_planeRight.normal)*_planeRight.normal;
			_gameElements.getGameElement(i)._translate += correcPos;
			_gameElements.getGameElement(i)._velocity += correcVel;
		}

		disact = _planeLeft.distPoint2Plane(_gameElements.getGameElement(i)._translate) + 0.05f;

		if (disantPL*disact < 0.0f) {
			//system("pause");
			correcPos = -(1.0f + 1.0f) * disact * _planeLeft.normal;
			correcVel = -(1.0f + 1.0f) * ((_gameElements.getGameElement(i))._velocity*_planeLeft.normal)*_planeLeft.normal;
			_gameElements.getGameElement(i)._translate += correcPos;
			_gameElements.getGameElement(i)._velocity += correcVel;
		}

		disact = _planeFront.distPoint2Plane(_gameElements.getGameElement(i)._translate) + 0.05f;

		if (disantPF*disact < 0.0f) {
			//system("pause");
			correcPos = -(1.0f + 1.0f) * disact * _planeFront.normal;
			correcVel = -(1.0f + 1.0f) * ((_gameElements.getGameElement(i))._velocity*_planeFront.normal)*_planeFront.normal;
			_gameElements.getGameElement(i)._translate += correcPos;
			_gameElements.getGameElement(i)._velocity += correcVel;
		}

		disact = _planeBack.distPoint2Plane(_gameElements.getGameElement(i)._translate) + 0.05f;

		if (disantPBa*disact < 0.0f) {
			//system("pause");
			correcPos = -(1.0f + 1.0f) * disact * _planeBack.normal;
			correcVel = -(1.0f + 1.0f) * ((_gameElements.getGameElement(i))._velocity*_planeBack.normal)*_planeBack.normal;
			_gameElements.getGameElement(i)._translate += correcPos;
			_gameElements.getGameElement(i)._velocity += correcVel;
		}
		//system("pause");
		
		
	}

	for (int i = 1; i < _gameElements.getNumGameElements() - 1;i++) {
		float dist = glm::length((_gameElements.getGameElement(i))._translate - (_gameElements.getGameElement(0))._translate);
		
		/*float diff = (dist - _gameElements.getGameElement(i)._scale.x);*/


		if (dist < (_gameElements.getGameElement(i)._scale.x + _gameElements.getGameElement(0)._scale.x)/2) {
			_gameState = GameState::EXIT;
		}
		break;
	}

	_gameElements.getGameElement(_gameElements.getNumGameElements() - 2)._translate = _gameElements.getGameElement(0)._translate;
	_gameElements.getGameElement(_gameElements.getNumGameElements() - 2)._translate.z += 0.3f;

	
	//CAMERA!!!
	glm::vec3 cameraPos = (_gameElements.getGameElement(0))._translate;
	_cam.setCameraFront(cameraPos);
	cameraPos += camOfsetX;
	//cameraPos.z += camOfsetZ;
	_cam.setCameraPos(cameraPos);


	//SCORE!!!
	if (counter >= 60) {
		score++;
		counter = 0;
	}

	counter++;
}

/**
* Draw the sprites on the screen
*/
void Game::renderGame() {
	GameObject currentElement;
	//Clear the color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//Bind the GLSL program. Only one code GLSL can be used at the same time
	_colorProgram.use();
	//Activate and Bind Texture
	glActiveTexture(GL_TEXTURE0);

	glUniformMatrix4fv(_viewMatrixUniform, 1, GL_FALSE, glm::value_ptr(_cam.getViewMatrix()));
	if (typeOfCamera == 0) {
		_cam.setNear(0.1f);
		camOfsetX = 3.0f;
		camOfsetZ = 0.5f;
		glUniformMatrix4fv(_projectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(_cam.getPrespectiveProjectionMatrix()));
	}
	else if (typeOfCamera == 1) {
		_cam.setNear(-30.0f);
		camOfsetX = 0.03f;
		camOfsetZ = 0.005f;
		glUniformMatrix4fv(_projectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(_cam.getOrtographicProjectionMatrix()));
	}

	glUniform1i(_lightMode, _lightModeSwitch);
	glUniform1i(_lightingEnabled, _lightSwitch);
	glUniform3fv(_viewerPosition, 1, glm::value_ptr(_cam.getCameraPos()));

	material currentMaterial;
	currentElement = _gameElements.getGameElement(_gameElements.getNumGameElements() - 2);
	glUniform3fv(_lightPosition, 1, glm::value_ptr(currentElement._translate));
			
	currentMaterial = _materialManager.getMaterialComponents(_materialManager.getMaterialID("light"));
	glUniform3fv(_lightAmbientUniform, 1, glm::value_ptr(currentMaterial.ambient));
	glUniform3fv(_lightDiffuseUniform, 1, glm::value_ptr(currentMaterial.diffuse));
	glUniform3fv(_lightSpecularUniform, 1, glm::value_ptr(currentMaterial.specular));
	glUniform3fv(_lightAttenuationUniform, 1, glm::value_ptr(currentMaterial.attenuation));

	currentElement = _gameElements.getGameElement(_gameElements.getNumGameElements() - 3);
	glUniform3fv(_lightPosition2, 1, glm::value_ptr(currentElement._translate));

	currentMaterial = _materialManager.getMaterialComponents(_materialManager.getMaterialID("light"));
	glUniform3fv(_lightAmbientUniform2, 1, glm::value_ptr(currentMaterial.ambient));
	glUniform3fv(_lightDiffuseUniform2, 1, glm::value_ptr(currentMaterial.diffuse));
	glUniform3fv(_lightSpecularUniform2, 1, glm::value_ptr(currentMaterial.specular));
	glUniform3fv(_lightAttenuationUniform2, 1, glm::value_ptr(currentMaterial.attenuation));
	
	//For each one of the elements: Each object MUST BE RENDERED based on its position, rotation and scale data
	//Each object MUST BE RENDERED based on the position, rotation and scale
	for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
		currentElement = _gameElements.getGameElement(i);
		
		currentMaterial = _materialManager.getMaterialComponents(_materialManager.getMaterialID(currentElement._materialType));

		glUniform3fv(_materialAmbientUniform, 1, glm::value_ptr(currentMaterial.ambient));
		glUniform3fv(_materialDiffuseUniform, 1, glm::value_ptr(currentMaterial.diffuse));
		glUniform3fv(_materialSpecularUniform, 1, glm::value_ptr(currentMaterial.specular));
		glUniform1f(_materialShininessUniform, currentMaterial.shininess);
		
		if (currentElement._materialType == "light") {
			glUniform1i(_isALightSource, 1);
		}
		else {
			glUniform1i(_isALightSource, 0);
		}


		//Model transformation matrix will scale, rotate and translate the current object. This matrix is built in the inverse order of the operations
		glm::mat4 modelMatrix;
		
		modelMatrix = glm::translate(modelMatrix, currentElement._translate);
		if (currentElement._angle != 0) {
			modelMatrix = glm::rotate(modelMatrix, glm::radians(currentElement._angle), currentElement._rotation);
		}
		modelMatrix = glm::scale(modelMatrix, currentElement._scale);
		glUniformMatrix4fv(_modelMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelMatrix));

		glm::mat3 modelNormalMatrix = glm::mat3(glm::transpose(glm::inverse(modelMatrix)));

		glUniformMatrix4fv(_modelNormalMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelNormalMatrix));

		//Texture
		glBindTexture(GL_TEXTURE_2D, currentElement._textureID);

		glUniform4fv(_newColorUniform, 1, glm::value_ptr(currentElement._color));
		glUniform1i(_textureDataLocation, 0);		//This line is not needed if we use only 1 texture, it is sending the GL_TEXTURE0		
		if (currentElement._textureRepetion) {
			glUniform2f(_textureScaleFactorLocation, currentElement._scale.x, currentElement._scale.y);
		}
		else {
			glUniform2f(_textureScaleFactorLocation, 1.0f, 1.0f);
		}

		//Send data to GPU
		_openGLBuffers.sendDataToGPU(_gameElements.getData(currentElement._objectType), _gameElements.getNumVertices(currentElement._objectType));

		//Unbind the texture
		glBindTexture(GL_TEXTURE_2D, 0);
	}


	//Unbind the program
	_colorProgram.unuse();

	//Swap the display buffers (displays what was just drawn)
	_window.swapBuffer();
}


