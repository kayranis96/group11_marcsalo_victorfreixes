#pragma once
//Third-party libraries
#include <GL/glew.h>			//The OpenGL Extension Wrangler
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
class Cam
{
private:
	float _aspectRatio;
	glm::mat4 _projectionMatrix;
	glm::mat4 _viewMatrix;
	float _FOV;
	float _far;
	float _near;
	float _projectionWidth;
	float _projectionHeight;
	glm::vec3 _cameraPos;
	glm::vec3 _cameraFront;
	glm::vec3 _cameraUp;
	void calculateViewMatrix();
	void calculateOrtographicTransformationMatrix();
	void calculatePrespectiveTransformationMatrix();
public:
	Cam();
	~Cam();

	void setAspectRatio(float AspcR);
	float getAspectRatio();
	void setFov(float fov);
	float getFov();
	void setFar(float far);
	float getFar();
	void setNear(float near);
	float getNear();
	void setProjectionWidth(float pw);
	float getProjectionWidth();
	void setProjectionHeight(float ph);
	float getProjectionHeight();
	void setCameraPos(glm::vec3 cp);
	glm::vec3 getCameraPos();
	void setCameraFront(glm::vec3 cf);
	glm::vec3 getCameraFront();
	void setCameraUp(glm::vec3 cu);
	glm::vec3 getCameraUp();

	glm::mat4 getViewMatrix();

	glm::mat4 getOrtographicProjectionMatrix();
	glm::mat4 getPrespectiveProjectionMatrix();
};

