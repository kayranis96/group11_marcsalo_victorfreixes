#pragma once


//Third-party libraries
#include <GL/glew.h>			//The OpenGL Extension Wrangler
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "Window.h"
#include "GLSLProgram.h"
#include "FPSLimiter.h"
#include "OpenGLBuffers.h"
#include "Vertex.h"
#include "Geometry.h"
#include "InputManager.h"
#include "Cam.h"
#include "GeometryPlane.h"
#include "TextureManager.h"
#include "MaterialManager.h"

//Game has four possible states: INIT (Preparing environment), PLAY (Playing), EXIT (Exit from the game) or MENU (Game menu)
enum class GameState{INIT, PLAY, EXIT, MENU};

//This class manages the game execution
class Game {
	public:						
		Game(std::string windowTitle, int screenWidth, int screenHeight, bool enableLimiterFPS, int maxFPS, bool printFPS);	//Constructor
		~Game();					//Destructor
		void run();					//Game execution

	private:
			//Attributes	
		std::string _windowTitle;			//Window Title
		int _screenWidth;					//Screen width in pixels				
		int _screenHeight;					//Screen height in pixels				
		GameState _gameState;				//It describes the game state				
		Window _window;						//Manage the OpenGL context
		GLSLProgram _colorProgram;			//Manage the shader programs
		FPSLimiter _fpsLimiter;				//Manage the synchronization between frame rate and refresh rate
		OpenGLBuffers _openGLBuffers;		//Manage the openGL buffers
		Geometry _gameElements;				//Manage the game elements
		InputManager _inputManager;			//Manage the input devices
		TextureManager _textureManager;		//Manage all the data related to the different textures used in the game
		MaterialManager _materialManager;	//Manage all the data related to the different materials used in the game
		GLuint _modelMatrixUniform;
		GLuint _modelNormalMatrixUniform;
		GLuint _viewMatrixUniform;
		GLuint _projectionMatrixUniform;
		GLuint _newColorUniform;
		GLint _textureDataLocation;
		GLint _textureScaleFactorLocation;
		GLuint _lightAmbientUniform;
		GLuint _lightDiffuseUniform;
		GLuint _lightSpecularUniform;
		GLuint _lightAttenuationUniform;
		GLuint _lightAmbientUniform2;
		GLuint _lightDiffuseUniform2;
		GLuint _lightSpecularUniform2;
		GLuint _lightAttenuationUniform2;
		GLuint _materialAmbientUniform;
		GLuint _materialDiffuseUniform;
		GLuint _materialSpecularUniform;
		GLuint _materialShininessUniform;
		GLint _isALightSource;
		GLint _lightingEnabled;
		GLuint _lightPosition;
		GLuint _lightPosition2;
		GLuint _viewerPosition;
		GLint _lightMode;

		int _lightSwitch = 1;
		int _lightModeSwitch = 1;


		int typeOfCamera = 0;
		Cam _cam;
		glm::vec3 camOfset = glm::vec3(0.0f, 0.0f, 2.0f);
		float camOfsetX= 3.0f;
		float camOfsetZ = 0.5f;

		float _dt = 0.01f;
		bool _jump = false;
		glm::vec3 _gravity = glm::vec3(0, 0, -9.8);

		Plane _planeBottom;
		Plane _planeTop;
		Plane _planeRight;
		Plane _planeLeft;
		Plane _planeFront;
		Plane _planeBack;
		
		int score = 0;
		int counter = 0;


			//Internal methods
		void initSystems();
		void initShaders();	
		void loadGameTextures();
		void gameLoop();
		void processInput();
		void doPhysics();
		void executePlayerCommands();
		void renderGame();			
};

