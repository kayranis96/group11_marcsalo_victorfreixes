#include "Cam.h"



void Cam::calculateViewMatrix()
{
	glm::vec3 cameraDirection = glm::normalize(_cameraPos - _cameraFront);
	glm::vec3 up = glm::vec3(0.0f,0.0f,1.0f);
	glm::vec3 cameraRight = glm::normalize(glm::cross(up, cameraDirection));
	_cameraUp = glm::cross(cameraDirection, cameraRight);
	_viewMatrix = glm::lookAt(_cameraPos, _cameraFront, _cameraUp);
}

void Cam::calculateOrtographicTransformationMatrix()
{
	_projectionMatrix = glm::ortho(-_projectionWidth / 2, _projectionWidth / 2, -_projectionHeight / 2, _projectionHeight / 2, _near, _far);
}

void Cam::calculatePrespectiveTransformationMatrix()
{
	_projectionMatrix = glm::perspective(_FOV, _aspectRatio, _near, _far);
}

Cam::Cam()
{
}


Cam::~Cam()
{
}

void Cam::setAspectRatio(float AspcR)
{
	_aspectRatio = AspcR;
}

float Cam::getAspectRatio()
{
	return _aspectRatio;
}

void Cam::setFov(float fov)
{
	_FOV = fov;
}

float Cam::getFov()
{
	return _FOV;
}

void Cam::setFar(float far)
{
	_far = far;
}

float Cam::getFar()
{
	return _far;
}

void Cam::setNear(float near)
{
	_near = near;
}

float Cam::getNear()
{
	return _near;
}

void Cam::setProjectionWidth(float pw)
{
	_projectionWidth = pw;
}

float Cam::getProjectionWidth()
{
	return _projectionWidth;
}

void Cam::setProjectionHeight(float ph)
{
	_projectionHeight = ph;
}

float Cam::getProjectionHeight()
{
	return _projectionHeight;
}

void Cam::setCameraPos(glm::vec3 cp)
{
	_cameraPos = cp;
}

glm::vec3 Cam::getCameraPos()
{
	return _cameraPos;
}

void Cam::setCameraFront(glm::vec3 cf)
{
	_cameraFront = cf;
}

glm::vec3 Cam::getCameraFront()
{
	return _cameraFront;
}

void Cam::setCameraUp(glm::vec3 cu)
{
	_cameraUp = cu;
}

glm::vec3 Cam::getCameraUp()
{
	return _cameraUp;
}

glm::mat4 Cam::getViewMatrix()
{
	calculateViewMatrix();
	return _viewMatrix;
}

glm::mat4 Cam::getOrtographicProjectionMatrix()
{
	calculateOrtographicTransformationMatrix();
	return _projectionMatrix;
}

glm::mat4 Cam::getPrespectiveProjectionMatrix()
{
	calculatePrespectiveTransformationMatrix();
	return _projectionMatrix;
}
