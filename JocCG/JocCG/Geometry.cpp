#include "Geometry.h"
#include "ErrorManagement.h"
#include <iostream>
#include <fstream>

using namespace std;


/*
* Constructor
*/
Geometry::Geometry() {
	this->loadGameElements("./resources/scene3D.txt");
	//_verticesData[BLUE_CUBE] = new Vertex[36];
	_numVertices.resize(NUMBASICOBJECTS);
	_verticesData.resize(NUMBASICOBJECTS);
	//_loader.loadAse("../sharedResources/models/tankUP.ASE", _numVertices, _verticesData);
//	for (int i = 0; i < NUMBASICOBJECTS; i++)
	//{
		//if (_listOfObjects[i]._objectType == 0) {
			
		//_numVertices[PYRAMID] = 18;
		//_verticesData[PYRAMID] = new Vertex[_numVertices[PYRAMID]];

		///*
		//P3*****P4
		//* *	  * *
		//*   P0  *
		//* *   * *
		//P1*****P2

		//*/

		//// top: P1, P4, P2, P4, P1, P3
		//_verticesData[PYRAMID][0].setPosition(-0.5f, -0.5f, -0.5f);
		//_verticesData[PYRAMID][0].setUV(0, 0);
		//_verticesData[PYRAMID][1].setPosition(0.5f, 0.5f, -0.5f);
		//_verticesData[PYRAMID][1].setUV(1, 1);
		//_verticesData[PYRAMID][2].setPosition(0.5f, -0.5f, -0.5f);
		//_verticesData[PYRAMID][2].setUV(1, 0);
		//_verticesData[PYRAMID][3].setPosition(0.5f, 0.5f, -0.5f);
		//_verticesData[PYRAMID][3].setUV(1, 1);
		//_verticesData[PYRAMID][4].setPosition(-0.5f, -0.5f, -0.5f);
		//_verticesData[PYRAMID][4].setUV(0, 0);
		//_verticesData[PYRAMID][5].setPosition(-0.5f, 0.5f, -0.5f);
		//_verticesData[PYRAMID][5].setUV(0, 1);

		////bottom lateral side P0,P4, P3
		//_verticesData[PYRAMID][6].setPosition(0.0f, 0.0f, 0.5f);
		//_verticesData[PYRAMID][6].setUV(0.5f, 1.0f);
		//_verticesData[PYRAMID][7].setPosition(0.5f, 0.5f, -0.5f);
		//_verticesData[PYRAMID][7].setUV(0.0f, 0.0f);
		//_verticesData[PYRAMID][8].setPosition(-0.5f, 0.5f, -0.5f);
		//_verticesData[PYRAMID][8].setUV(1.0f, 0.0f);

		////left lateral side P0, P3, P1
		//_verticesData[PYRAMID][9].setPosition(0.0f, 0.0f, 0.5f);
		//_verticesData[PYRAMID][9].setUV(0.5f, 1.0f);
		//_verticesData[PYRAMID][10].setPosition(-0.5f, 0.5f, -0.5f);
		//_verticesData[PYRAMID][10].setUV(0.0f, 0.0f);
		//_verticesData[PYRAMID][11].setPosition(-0.5f, -0.5f, -0.5f);
		//_verticesData[PYRAMID][11].setUV(1.0f, 0.0f);


		////top lateral side P0, P1, P2
		//_verticesData[PYRAMID][12].setPosition(0.0f, 0.0f, 0.5f);
		//_verticesData[PYRAMID][12].setUV(0.5f, 1.0f);
		//_verticesData[PYRAMID][13].setPosition(-0.5f, -0.5f, -0.5f);
		//_verticesData[PYRAMID][13].setUV(0.0f, 0.0f);
		//_verticesData[PYRAMID][14].setPosition(0.5f, -0.5f, -0.5f);
		//_verticesData[PYRAMID][14].setUV(1.0f, 0.0f);

		////right lateral side, P0, P2, P4
		//_verticesData[PYRAMID][15].setPosition(0.0f, 0.0f, 0.5f);
		//_verticesData[PYRAMID][15].setUV(0.5f, 1.0f);
		//_verticesData[PYRAMID][16].setPosition(0.5f, -0.5f, -0.5f);
		//_verticesData[PYRAMID][16].setUV(0.0f, 0.0f);
		//_verticesData[PYRAMID][17].setPosition(0.5f, 0.5f, -0.5f);
		//_verticesData[PYRAMID][17].setUV(1.0f, 0.0f);





			

		_numVertices[CUBE] = 36;
		_verticesData[CUBE] = new Vertex[_numVertices[CUBE]];
		
		glm::vec3 U;
		glm::vec3 V;
		glm::vec3 N;

		//TOP
		_verticesData[CUBE][0].setPosition(1.0f, 1.0f, -1.0f);
		_verticesData[CUBE][0].setUV(1.0f, 0.0f);
		_verticesData[CUBE][1].setPosition(-1.0f, 1.0f, -1.0f);
		_verticesData[CUBE][1].setUV(0.0f, 0.0f);
		_verticesData[CUBE][2].setPosition(-1.0f, -1.0f, -1.0f);
		_verticesData[CUBE][2].setUV(0.5f, 1.0f);
		_verticesData[CUBE][3].setPosition(-1.0f, -1.0f, -1.0f);
		_verticesData[CUBE][3].setUV(1.0f, 0.0f);
		_verticesData[CUBE][4].setPosition(1.0f, -1.0f, -1.0f);
		_verticesData[CUBE][4].setUV(0.0f, 0.0f);
		_verticesData[CUBE][5].setPosition(1.0f, 1.0f, -1.0f);
		_verticesData[CUBE][5].setUV(0.5f, 1.0f);
		//BOT
		_verticesData[CUBE][6].setPosition(1.0f, 1.0f, 1.0f);
		_verticesData[CUBE][6].setUV(1.0f, 0.0f);
		_verticesData[CUBE][7].setPosition(1.0f, -1.0f, 1.0f);
		_verticesData[CUBE][7].setUV(0.0f, 0.0f);
		_verticesData[CUBE][8].setPosition(-1.0f, -1.0f, 1.0f);
		_verticesData[CUBE][8].setUV(0.5f, 1.0f);
		_verticesData[CUBE][9].setPosition(-1.0f, -1.0f, 1.0f);
		_verticesData[CUBE][9].setUV(1.0f, 0.0f);
		_verticesData[CUBE][10].setPosition(-1.0f, 1.0f, 1.0f);
		_verticesData[CUBE][10].setUV(0.0f, 0.0f);
		_verticesData[CUBE][11].setPosition(1.0f, 1.0f, 1.0f);
		_verticesData[CUBE][11].setUV(0.5f, 1.0f);
		//FRONT
		_verticesData[CUBE][12].setPosition(1.0f, -1.0f, -1.0f);
		_verticesData[CUBE][12].setUV(1.0f, 0.0f);
		_verticesData[CUBE][13].setPosition(-1.0f, -1.0f, -1.0f);
		_verticesData[CUBE][13].setUV(0.0f, 0.0f);
		_verticesData[CUBE][14].setPosition(-1.0f, -1.0f, 1.0f);
		_verticesData[CUBE][14].setUV(0.5f, 1.0f);
		_verticesData[CUBE][15].setPosition(-1.0f, -1.0f, 1.0f);
		_verticesData[CUBE][15].setUV(1.0f, 0.0f);
		_verticesData[CUBE][16].setPosition(-1.0f, -1.0f, 1.0f);
		_verticesData[CUBE][16].setUV(0.0f, 0.0f);
		_verticesData[CUBE][17].setPosition(1.0f, -1.0f, -1.0f);
		_verticesData[CUBE][17].setUV(0.5f, 1.0f);
		//BACK
		_verticesData[CUBE][18].setPosition(1.0f, 1.0f, -1.0f);
		_verticesData[CUBE][18].setUV(1.0f, 0.0f);
		_verticesData[CUBE][19].setPosition(1.0f, 1.0f, 1.0f);
		_verticesData[CUBE][19].setUV(0.0f, 0.0f);
		_verticesData[CUBE][20].setPosition(-1.0f, 1.0f, 1.0f);
		_verticesData[CUBE][20].setUV(0.5f, 1.0f);
		_verticesData[CUBE][21].setPosition(-1.0f, 1.0f, 1.0f);
		_verticesData[CUBE][21].setUV(1.0f, 0.0f);
		_verticesData[CUBE][22].setPosition(-1.0f, 1.0f, -1.0f);
		_verticesData[CUBE][22].setUV(0.0f, 0.0f);
		_verticesData[CUBE][23].setPosition(1.0f, 1.0f, -1.0f);
		_verticesData[CUBE][23].setUV(0.5f, 1.0f);
		//LEFT
		_verticesData[CUBE][24].setPosition(-1.0f, 1.0f, -1.0f);
		_verticesData[CUBE][24].setUV(1.0f, 0.0f);
		_verticesData[CUBE][25].setPosition(-1.0f, 1.0, 1.0f);
		_verticesData[CUBE][25].setUV(0.0f, 0.0f);
		_verticesData[CUBE][26].setPosition(-1.0f, -1.0f, 1.0f);
		_verticesData[CUBE][26].setUV(0.5f, 1.0f);
		_verticesData[CUBE][27].setPosition(-1.0f, -1.0f, 1.0f);
		_verticesData[CUBE][27].setUV(1.0f, 0.0f);
		_verticesData[CUBE][28].setPosition(-1.0f, -1.0f, -1.0f);
		_verticesData[CUBE][28].setUV(0.0f, 0.0f);
		_verticesData[CUBE][29].setPosition(-1.0f, 1.0f, -1.0f);
		_verticesData[CUBE][29].setUV(0.5f, 1.0f);
		//RIGHT
		_verticesData[CUBE][30].setPosition(1.0f, 1.0f, -1.0f);
		_verticesData[CUBE][30].setUV(1.0f, 0.0f);
		_verticesData[CUBE][31].setPosition(1.0f, -1.0f, -1.0f);
		_verticesData[CUBE][31].setUV(0.0f, 0.0f);
		_verticesData[CUBE][32].setPosition(1.0f, -1.0f, 1.0f);
		_verticesData[CUBE][32].setUV(1.0f, 0.0f);
		_verticesData[CUBE][33].setPosition(1.0f, -1.0f, 1.0f);
		_verticesData[CUBE][33].setUV(1.0f, 0.0f);
		_verticesData[CUBE][34].setPosition(1.0f, 1.0f, 1.0f);
		_verticesData[CUBE][34].setUV(0.0f, 0.0f);
		_verticesData[CUBE][35].setPosition(1.0f, 1.0f, -1.0f);
		_verticesData[CUBE][35].setUV(0.5f, 1.0f);

		for (int i = 0; i < _numVertices[CUBE]-3; i += 3) {
			U = glm::vec3(_verticesData[CUBE][i + 1].position.x, _verticesData[CUBE][i + 1].position.y, _verticesData[CUBE][i + 1].position.z)
				- glm::vec3(_verticesData[CUBE][i].position.x, _verticesData[CUBE][i].position.y, _verticesData[CUBE][i].position.z);
			V = glm::vec3(_verticesData[CUBE][i + 2].position.x, _verticesData[CUBE][i + 2].position.y, _verticesData[CUBE][i + 2].position.z)
				- glm::vec3(_verticesData[CUBE][i].position.x, _verticesData[CUBE][i].position.y, _verticesData[CUBE][i].position.z);

			N = glm::cross(U, V);
			_verticesData[CUBE][i].setNormal(N.x,N.y,N.z);
			_verticesData[CUBE][i + 1].setNormal(N.x, N.y, N.z);
			_verticesData[CUBE][i + 2].setNormal(N.x, N.y, N.z);
		}
		

		
		/*for (int j = 0; j < _numVertices[CUBE];j++) {
			_verticesData[CUBE][j].setColor(255, 255, 255, 255);
		}*/
		
		_loader.loadAse("../sharedResources/models/teapot.ASE", _numVertices, _verticesData);

		for (int i = 0; i < _numVertices[TEAPOT] - 3; i += 3) {
			U = glm::vec3(_verticesData[TEAPOT][i + 1].position.x, _verticesData[TEAPOT][i + 1].position.y, _verticesData[TEAPOT][i + 1].position.z)
				- glm::vec3(_verticesData[TEAPOT][i].position.x, _verticesData[TEAPOT][i].position.y, _verticesData[TEAPOT][i].position.z);
			V = glm::vec3(_verticesData[TEAPOT][i + 2].position.x, _verticesData[TEAPOT][i + 2].position.y, _verticesData[TEAPOT][i + 2].position.z)
				- glm::vec3(_verticesData[TEAPOT][i].position.x, _verticesData[TEAPOT][i].position.y, _verticesData[TEAPOT][i].position.z);

			N = glm::cross(U, V);
			_verticesData[TEAPOT][i].setNormal(N.x, N.y, N.z);
			_verticesData[TEAPOT][i + 1].setNormal(N.x, N.y, N.z);
			_verticesData[TEAPOT][i + 2].setNormal(N.x, N.y, N.z);
		}

		/*for (int j = 0; j < _numVertices[TEAPOT];j++) {
			_verticesData[TEAPOT][j].setColor(255, 255, 255, 255);
		}*/
		

		_loader.loadAse("../sharedResources/models/tankBOTTOM.ASE", _numVertices, _verticesData);
		
		for (int i = 0; i < _numVertices[OBJ] - 3; i += 3) {
			U = glm::vec3(_verticesData[OBJ][i + 1].position.x, _verticesData[OBJ][i + 1].position.y, _verticesData[OBJ][i + 1].position.z)
				- glm::vec3(_verticesData[OBJ][i].position.x, _verticesData[OBJ][i].position.y, _verticesData[OBJ][i].position.z);
			V = glm::vec3(_verticesData[OBJ][i + 2].position.x, _verticesData[OBJ][i + 2].position.y, _verticesData[OBJ][i + 2].position.z)
				- glm::vec3(_verticesData[OBJ][i].position.x, _verticesData[OBJ][i].position.y, _verticesData[OBJ][i].position.z);

			N = glm::cross(U, V);
			_verticesData[OBJ][i].setNormal(N.x, N.y, N.z);
			_verticesData[OBJ][i + 1].setNormal(N.x, N.y, N.z);
			_verticesData[OBJ][i + 2].setNormal(N.x, N.y, N.z);
		}

		/*for (int j = 0; j < _numVertices[OBJ];j++) {
			_verticesData[OBJ][j].setColor(255, 255, 255, 255);
		}*/

}


Geometry::~Geometry() {
	for (int i = 0;i < NUMBASICOBJECTS;i++) {
		//erase vertexes cube

		delete[] _verticesData[i];


	}
	_listOfObjects.clear();
}

/*
* Load the game elements from a text file
*/
void Geometry::loadGameElements(char fileName[100]) {
	/* Text format
	<number of game elements>
	<type of game element> <vec3 position> <angle> <vec3 rotation> <vec3 scale>
	*/
	int numGameElements;
	GameObject tempObject;
	glm::vec3 vector3fElements;
	ifstream file;
	file.open(fileName);

	if (file.is_open()) {
		//TODO: Read the content and add it into the data structure
		std::string path;
		int textRep;
		file >> numGameElements;		
		std::cout << numGameElements << std::endl;
		tempObject._textureFile = path;
		for (int i = 0; i < numGameElements; i++) {
			file >>
			tempObject._objectType >>
			tempObject._color.r >> tempObject._color.g >> tempObject._color.b >> tempObject._color.a >>
			textRep;

			tempObject._color.r/=255;
			tempObject._color.g/=255;
			tempObject._color.b/=255;
			tempObject._color.a/=255;

			file >> tempObject._materialType;

			file >> path;
			if (textRep == 0) {
				tempObject._textureRepetion = false;
					
			}
			else {
				tempObject._textureRepetion = true;
					
			}
				
			tempObject._textureFile = path;

			std::cout << tempObject._textureFile << std::endl;
			std::cout << std::endl;

			file >>
			tempObject._translate.x >> tempObject._translate.y >> tempObject._translate.z >>
			tempObject._angle >>
			tempObject._rotation.x >> tempObject._rotation.y >> tempObject._rotation.z >>
			tempObject._scale.x >> tempObject._scale.y >> tempObject._scale.z;
			_listOfObjects.push_back(tempObject);
			
		}
		file.close();

	}
	else {
		string message = "The file " + string(fileName) + " doesn't exists";
		ErrorManagement::errorRunTime(message);
	}

}

/*
* Get the vertices data for an specific object
* @param objectID is the identifier of the requested object
* @return Vertex * is an array with all the vertices data
*/
Vertex * Geometry::getData(int objectID) {
	return _verticesData[objectID];
}

/*
* Get the number of vertices for an specific object
* @param objectID is the identifier of the requested object
* @return int is the number of vertices
*/

int Geometry::getNumVertices(int objectID) {
	return _numVertices[objectID];
}

/*
* Get the number of elements to render
*/
int Geometry::getNumGameElements() {
	return _listOfObjects.size();
}

/*
* Get the number of vertices of an specific game element
* @param objectID is the identifier of the requested object
*/
GameObject & Geometry::getGameElement(int objectID) {
	return (_listOfObjects[objectID]);
}

