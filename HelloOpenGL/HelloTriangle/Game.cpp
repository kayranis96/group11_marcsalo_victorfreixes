#include "Game.h"

#define ActualTime SDL_GetTicks()

/**
* Constructor
* Note: It uses an initialization list to set the parameters
* @param windowTitle is the window title
* @param screenWidth is the window width
* @param screenHeight is the window height
*/
Game::Game(std::string windowTitle, int screenWidth, int screenHeight, bool enableLimiterFPS, int maxFPS, bool printFPS) :
	_windowTitle(windowTitle),
	_screenWidth(screenWidth),
	_screenHeight(screenHeight),
	_gameState(GameState::INIT),
	_fpsLimiter(enableLimiterFPS, maxFPS, printFPS) {
}

/**
* Destructor
*/
Game::~Game()
{
	
}

/*
* Game execution
*/
void Game::run() {
	//System initializations
	initSystems();
	//Start the game if all the elements are ready
	gameLoop();
}

/*
* Initializes all the game engine components
*/
void Game::initSystems() {
	//Create an Opengl window using SDL
	_window.create(_windowTitle, _screenWidth, _screenHeight, 0);
	//Compile and Link shader
	loadShaders();
	//Load the current scenario
	_openGLBuffers.initializeBuffers(_colorProgram);
	//Create several vertex
	createPrimitivesToRender();
}

/*
* Compiles, sets the variables between C++ and the Shader program and links the shader program
*/
void Game::loadShaders() {
	//Compile the shaders
	_colorProgram.addShader(GL_VERTEX_SHADER, "./resources/shaders/vertex-shader.cpp");
	_colorProgram.addShader(GL_FRAGMENT_SHADER, "./resources/shaders/fragment-shader.cpp");
	_colorProgram.compileShaders();
	//Attributes must be added before linking the code
	_colorProgram.addAttribute("vertexPosition");
	_colorProgram.addAttribute("vertexColor");
	//Link the compiled shaders
	_colorProgram.linkShaders();
}

/*
* Loads all the element that has to be displayed on the screen
*/
void Game::createPrimitivesToRender() {
	
	data[0].setPosition(0.0f, 0.8f, 0.0f);		//top
	data[0].setColor(218, 165, 32, 255);

	data[1].setPosition(-0.35f, 0.0f, 0.0f);	//bottom left
	data[1].setColor(218, 165, 32, 255);

	data[2].setPosition(0.35f, 0.0f, 0.0f);		//bottom right
	data[2].setColor(218, 165, 32, 255);

	
	data[3].setPosition(-0.35f, 0.0f, 0.0f);	//top
	data[3].setColor(218, 165, 32, 255);

	data[4].setPosition(-0.7f, -0.8f, 0.0f);	//bottom left
	data[4].setColor(218, 165, 32, 255);

	data[5].setPosition(-0.0f, -0.8f, 0.0f);	//bottom right	
	data[5].setColor(218, 165, 32, 255);

	
	data[6].setPosition(0.35f, 0.0f, 0.0f);		//top
	data[6].setColor(218, 165, 32, 255);

	data[7].setPosition(0.0f, -0.8f, 0.0f);		//bottom left
	data[7].setColor(218, 165, 32, 255);

	data[8].setPosition(0.7f, -0.8f, 0.0f);		//bottom right
	data[8].setColor(218, 165, 32, 255);

	////Set the color to magenta
	//for (int i = 0; i < 6; i++) {
	//	data[i].setColor(255, 255, 255, 255);
	//}
	////Set the color coordinate to blue
	//data[0].setColor(0, 0, 255, 255);
	//data[3].setColor(0, 0, 255, 255);

	////Set the color coordinate to green
	//data[1].setColor(0, 255, 0, 255);
	//data[4].setColor(0, 255, 0, 255);

	
	
}


/*
* Game execution: Gets input events, processes game logic and draws sprites on the screen
*/
void Game::gameLoop() {
	_gameState = GameState::PLAY;
	while (_gameState != GameState::EXIT) {
		//Start synchronization between refresh rate and frame rate
		_fpsLimiter.startSynchronization();
		//Process the input information (keyboard and mouse)
		processInput();
		//Execute pending actions
		updateGameObjects();
		//Draw the objects on the screen
		drawGame();
		//Force synchronization
		_fpsLimiter.forceSynchronization();
	}
	
}

/*
* Processes input with SDL
*/
void Game::processInput() {
	//Review https://wiki.libsdl.org/SDL_Event to see the different kind of events
	//Moreover, table show the property affected for each event type
	SDL_Event evnt;
	//Will keep looping until there are no more events to process
	while (SDL_PollEvent(&evnt)) {
		switch (evnt.type) {
		case SDL_QUIT:
			_gameState = GameState::EXIT;
			break;
		default:
			break;
		}
	}
}

/**
* Draw the sprites on the screen
*/
void Game::drawGame() {
	//Clear the color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Bind the GLSL program. Only one code GLSL can be used at the same time
	_colorProgram.use();

	//Set the new value for the uniform variable
	//Important: If time is not used in the shader, we will get an error because GLSL will delete it for us. Then, we need to comment the following three lines
	_time = _time + 0.05f;
	GLuint timeLocation = _colorProgram.getUniformLocation("time");
	GLuint dynamicColorLocation = _colorProgram.getUniformLocation("dynamicColor");
	GLuint modelMatrixUniform = _colorProgram.getUniformLocation("modelMatrix");

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelMatrix));

	//Send data to GPU
	glUniform1f(timeLocation, _time);

	glUniform1i(dynamicColorLocation, 1);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	_openGLBuffers.sendDataToGPU(data, MAX_VERTICES , GL_TRIANGLES);
	/*glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	_openGLBuffers.sendDataToGPU(&(data[3]), 3, GL_TRIANGLES);
	glUniform1i(dynamicColorLocation, 0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	_openGLBuffers.sendDataToGPU(&(data[6]), 3, GL_TRIANGLES);*/
	

	//Unbind the program
	_colorProgram.unuse();

	//Swap the display buffers (displays what was just drawn)
	_window.swapBuffer();
}

void Game::updateGameObjects() {

	/*
	glm::mat4 trans;
	trans = glm::rotate(trans, glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	glm::vec4 result = trans * glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	printf("%f, %f, %f\n", result.x, result.y, result.z);
	*/

	//glm::mat4 identityMatrix;
	//modelMatrix = identityMatrix;
	//The animation consists in randomly rotating the triangle. The operations must be done in the inverse order that we want to do
	//Scale the triangle
	//modelMatrix = glm::scale(identityMatrix, glm::vec3(1.25, 1, 1));
	static int counter = 0;
	//if (counter == 5) {
		//3rd step: Restore the triangle to its original position
		modelMatrix = glm::translate(modelMatrix, glm::vec3(triangle3DPosition.x, triangle3DPosition.y, 0.0f));

		//2nd: Rotate 90� along the z-axis
		modelMatrix = glm::rotate(modelMatrix, glm::radians(45.0f), glm::vec3(0.0f, 0.0f, 1.0f));

		//1st: Move the triangle to the center
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-triangle3DPosition.x, -triangle3DPosition.y, 0.0f));
		counter = 0;
	//}
	counter++;
}

